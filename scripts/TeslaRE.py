#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import re
import requests
import datetime
 
url = 'http://www.nasdaq.com/es/symbol/tsla/real-time'


r = requests.get(url)

patron1 = re.compile(r'\$(\d+.\d+|\d+,\d+|\d+)')
patron2 = re.compile(r'\d+:\d{2}:\d{2}')

valor = patron1.findall(r.text)[0]
hora = patron2.findall(r.text)[0]

print valor + ' ' + hora
