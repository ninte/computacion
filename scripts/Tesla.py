#!/usr/bin/env python
#-*- coding: UTF-8 -*-

from bs4 import BeautifulSoup
import requests
import datetime
 
url = 'http://www.nasdaq.com/es/symbol/tsla/real-time'


r = requests.get(url)

soup = BeautifulSoup(r.text, 'html.parser')

valor = soup.find('div', class_='qwidget-dollar').text.lstrip('$')
hora = soup.find('div', class_='floatL paddingB15px').text.split(' ')[15]

print valor + ' ' + hora
