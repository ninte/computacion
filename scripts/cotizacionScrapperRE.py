#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import re
import requests
import MySQLdb
import datetime
import time
from beebotte import *

accesskey  = '0f16905f98d575977b7d5e949da9504e'
secretkey  = '9b21c5f6f62e0d81d0f21a6bdecd7abb87c5562169371797d6b05525659ce176'
hostname   = 'api.beebotte.com'
bbt = BBT( accesskey, secretkey, hostname = hostname)

DB_HOST = 'localhost' 
DB_USER = 'root' 
DB_PASS = 'root' 
DB_NAME = 'bolsa' 

url = 'http://www.nasdaq.com/es/symbol/tsla/real-time'

def run_query(query=''): 
    datos = [DB_HOST, DB_USER, DB_PASS, DB_NAME] 
 
    conn = MySQLdb.connect(*datos) # Conectar a la base de datos 
    cursor = conn.cursor()         # Crear un cursor 
    cursor.execute(query)          # Ejecutar una consulta 
 
    if query.upper().startswith('SELECT'): 
        data = cursor.fetchall()   # Traer los resultados de un select 
    else: 
        conn.commit()              # Hacer efectiva la escritura de datos 
        data = None 
 
    cursor.close()                 # Cerrar el cursor 
    conn.close()                   # Cerrar la conexión 
 
    return data

while(1):
    r = requests.get(url)

    patron1 = re.compile(r'\$(\d+.\d+|\d+,\d+|\d+)')
    patron2 = re.compile(r'\d:\d{2}:\d{2}')

    valor = patron1.findall(r.text)[0]
    hora = patron2.findall(r.text)[0]
    print valor + ' ' + hora

    query = "INSERT INTO TESLA (cotizacion, fecha) VALUES (%f, '%s')" %(float(valor), time.strftime("%Y/%m/%d") + ' ' + hora)
    run_query(query)

    bbt.write("Tesla", "Valor", float(valor))
    bbt.write("Tesla", "Fecha", time.strftime("%Y/%m/%d") + ' ' + hora)

    time.sleep(30)
