#!/usr/bin/env python
#-*- coding: UTF-8 -*-

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from flask import Flask, request, render_template, send_from_directory
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import MySQLdb, os, time, atexit, requests
from beebotte import *

DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = 'root'
DB_NAME = 'bolsa'

accesskey  = '0f16905f98d575977b7d5e949da9504e'
secretkey  = '9b21c5f6f62e0d81d0f21a6bdecd7abb87c5562169371797d6b05525659ce176'
hostname   = 'api.beebotte.com'
bbt = BBT( accesskey, secretkey, hostname = hostname)

url = 'http://www.nasdaq.com/es/symbol/tsla/real-time'

app = Flask(__name__)

def run_query(query=''): 
	datos = [DB_HOST, DB_USER, DB_PASS, DB_NAME] 
 
	conn = MySQLdb.connect(*datos) # Conectar a la base de datos 
	cursor = conn.cursor()		 # Crear un cursor 
	cursor.execute(query)		  # Ejecutar una consulta 
 
	if query.upper().startswith('SELECT'): 
		data = cursor.fetchall()   # Traer los resultados de un select 
	else: 
		conn.commit()			  # Hacer efectiva la escritura de datos 
		data = None 
 
	cursor.close()				 # Cerrar el cursor 
	conn.close()				   # Cerrar la conexión 
 
	return data

def get_cotizacion():
	r = requests.get(url)
	soup = BeautifulSoup(r.text, 'html.parser')
	valor = soup.find('div', class_='qwidget-dollar').text.lstrip('$')
	tiempo = soup.find('div', class_='floatL paddingB15px').text.split(' ')
	if(len(tiempo) >15):
		fecha = time.strftime("%Y/%m/%d") + ' ' + tiempo[15] + ' ' + tiempo[16][0:2]
		fecha = datetime.strptime(fecha,'%Y/%m/%d %I:%M:%S %p') + timedelta(hours=6)

		query = "INSERT INTO TESLA (cotizacion, fecha) VALUES (%f, '%s')" %(float(valor), fecha)
		run_query(query)

		bbt.write("Tesla", "Valor", float(valor))
		bbt.write("Tesla", "Fecha", str(fecha))
	else:
		print "Cotización cerrada"

def comprobar_umbral():
	global umbral
	global valor_referencia
	global lista_superior
	global lista_inferior

	limite_superior = valor_referencia+valor_referencia*umbral/100
	limite_inferior = valor_referencia-valor_referencia*umbral/100

	query_superior = "SELECT * FROM TESLA WHERE fecha BETWEEN '%s 00:00:00' AND '%s 23:59:59' AND cotizacion >= %f ORDER BY fecha" %(time.strftime("%Y/%m/%d"),time.strftime("%Y/%m/%d"),float(limite_superior))
	query_inferior = "SELECT * FROM TESLA WHERE fecha BETWEEN '%s 00:00:00' AND '%s 23:59:59' AND cotizacion < %f ORDER BY fecha" %(time.strftime("%Y/%m/%d"),time.strftime("%Y/%m/%d"),float(limite_inferior))

	lista_superior = run_query(query_superior)
	lista_inferior = run_query(query_inferior)


scheduler = BackgroundScheduler()
scheduler.start()
scheduler.add_job(
	func=get_cotizacion,
	trigger=IntervalTrigger(seconds=30),
	id='cotizacion_job',
	name='Get cotizacion Tesla',
	replace_existing=True)
# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())


@app.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(app.root_path, 'static'),
							   'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/', methods=['GET', 'POST'])
def index():
	global umbral
	global valor_medio
	global origen
	global flag

	if(request.method == 'GET'):
		query = run_query('SELECT * FROM TESLA ORDER BY fecha DESC LIMIT 1')
		comprobar_umbral()
		return render_template(
			'index.html',
			valor = query[0][1], fecha = query[0][2].strftime('%Y-%m-%d %H:%M:%S'), simbolo = '$', umbral = umbral,
			valor_referencia = valor_referencia, fecha_referencia = fecha_referencia, lista_superior = lista_superior,
			lista_inferior = lista_inferior, valor_medio = valor_medio, origen = origen
		)

	else:
		if(request.form['boton'] == 'Valor medio'):
			if(flag):
				valor_medio = float(run_query("SELECT avg(cotizacion) FROM TESLA WHERE fecha BETWEEN '%s 00:00:00' AND '%s 23:59:59'" %(time.strftime("%Y/%m/%d"),time.strftime("%Y/%m/%d")))[0][0])
				valor_medio = round(valor_medio,3)
				origen = 'Local'
				flag = 0
			else:
				cotizaciones = bbt.read("Tesla", "Valor", limit=60)
				valor_medio = 0.0
				for i in range(0,60):
					valor_medio = valor_medio + cotizaciones[i]['data']

				valor_medio = valor_medio/60
				valor_medio = round(valor_medio,3)
				origen = 'Externa'
				flag = 1

			query = run_query('SELECT * FROM TESLA ORDER BY fecha DESC LIMIT 1')
			return render_template(
				'index.html',
				valor = query[0][1], fecha = query[0][2].strftime('%Y-%m-%d %H:%M:%S'), simbolo = '$', umbral = umbral,
				valor_referencia = valor_referencia, fecha_referencia = fecha_referencia, lista_superior = lista_superior,
				lista_inferior = lista_inferior, valor_medio = valor_medio, origen = origen
			)
		else:
			umbral = float(request.form['umbral'])
			query = run_query('SELECT * FROM TESLA ORDER BY fecha DESC LIMIT 1')
			comprobar_umbral()
			return render_template(
				'index.html',
				valor = query[0][1], fecha = query[0][2].strftime('%Y-%m-%d %H:%M:%S'), simbolo = '$', umbral = umbral,
				valor_referencia = valor_referencia, fecha_referencia = fecha_referencia, lista_superior = lista_superior,
				lista_inferior = lista_inferior, valor_medio = valor_medio, origen = origen
			)

if __name__ == '__main__':
	global umbral
	global valor_referencia
	global fecha_referencia
	global valor_medio
	global origen
	global flag

	umbral = 1.5
	origen = 'Local'
	flag = 0
	get_cotizacion()
	valor_medio = float(run_query("SELECT avg(cotizacion) FROM TESLA WHERE fecha BETWEEN '%s 00:00:00' AND '%s 23:59:59'" %(time.strftime("%Y/%m/%d"),time.strftime("%Y/%m/%d")))[0][0])
	valor_medio = round(valor_medio,3)
	query = "SELECT * FROM TESLA WHERE fecha BETWEEN '%s 00:00:00' AND '%s 23:59:59' ORDER BY fecha ASC LIMIT 1" %(time.strftime("%Y/%m/%d"),time.strftime("%Y/%m/%d"))
	cotizacion_referencia = run_query(query)
	valor_referencia = cotizacion_referencia[0][1]
	fecha_referencia = cotizacion_referencia[0][2]
	comprobar_umbral()

	app.debug = False
	app.run( host = '0.0.0.0')